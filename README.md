# task21_quagga

OSPF
- Поднять три виртуалки
- Объединить их разными private network
1. Поднять OSPF между машинами на базе Quagga
2. Изобразить ассиметричный роутинг
3. Сделать один из линков "дорогим", но что бы при этом роутинг был симметричным
--------------------------------------------------------------------------------

OSPF поднял. Стенд выглядит как https://otus.ru/media/2e/30/Тестовый_стенд_к_занятию_по_маршрутизации-54017-2e30b4.jpg , но без R4 роутера
```
box.vm.provision "shell", inline: <<-SHELL 
          echo "sudo su -" >> .bashrc
          yum install -y epel-release
          yum install -y mc vim lsof nmap tcpdump quagga
          
          echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf                      # Включаю форвардинг
          echo "net.ipv6.conf.all.disable_ipv6=1" >> /etc/sysctl.conf           # Отключаю ipv6
          echo "net.ipv6.conf.default.disable_ipv6=1" >> /etc/sysctl.conf
          echo "net.ipv4.conf.eth1.rp_filter=2" >> /etc/sysctl.conf             # Не жесткая проверка rp_filter на портах
          echo "net.ipv4.conf.eth2.rp_filter=2" >> /etc/sysctl.conf
          sysctl -p

          sed -i s/^SELINUX=.*$/SELINUX=permissive/ /etc/selinux/config         # Отключаю selinux
          sudo setenforce 0                                            

          mv /vagrant/conf/$HOSTNAME"_lo" /etc/sysconfig/network-scripts/ifcfg-lo1 # Копирую конфиги интерфейсов, 
                                                                                # как пример каких то подключенных 
                                                                                # сетей

          mv /vagrant/conf/$HOSTNAME"_zebra.conf" /etc/quagga/zebra.conf        # Копирую конфиги на роутеры
          mv /vagrant/conf/$HOSTNAME"_ospfd.conf" /etc/quagga/ospfd.conf
          chown -R quagga: /etc/quagga                                          # Выставляю пользователя

          systemctl enable zebra ospfd                                          # Запускаю демоны
          systemctl start zebra ospfd
          reboot
        SHELL


```

Ассиметричный роутинг. Меняем косты на ro1
```
interface eth1
 ip ospf mtu-ignore
 ip ospf network point-to-point
 ip ospf hello-interval 5
 ip ospf dead-interval 10
 ip ospf cost 10
!
interface eth2
 ip ospf mtu-ignore
 ip ospf network point-to-point
 ip ospf hello-interval 5
 ip ospf dead-interval 10
 ip ospf cost 20
!
```
 Пингуем ro1 --> ro3 выглядит так: 
```
[root@ro1 quagga]# tracepath 10.20.0.1
 1?: [LOCALHOST]                                         pmtu 1500
 1:  10.0.0.2                                              1.275ms
 1:  10.0.0.2                                              0.781ms
 2:  10.20.0.1                                             1.784ms reached
     Resume: pmtu 1500 hops 2 back 1

```
в обратную сторону уже один хоп
```
[root@ro3 ~]# tracepath 10.10.0.1
 1?: [LOCALHOST]                                         pmtu 1500
 1:  10.10.0.1                                             0.520ms reached
 1:  10.10.0.1                                             0.813ms reached
     Resume: pmtu 1500 hops 1 back 1
```
Таблица маршрутизации выглядит так
```
[root@ro1 ~]# vtysh -c 'sh ip ospf route'
============ OSPF network routing table ============
N    10.0.0.0/30           [10] area: 0.0.0.0
                           directly attached to eth1
N    10.10.0.0/30          [20] area: 0.0.0.0
                           directly attached to eth2
N    10.20.0.0/30          [20] area: 0.0.0.0
                           via 10.0.0.2, eth1
N    127.10.0.0/16         [10] area: 0.0.0.0
                           directly attached to lo
N    127.20.0.0/16         [20] area: 0.0.0.0
                           via 10.0.0.2, eth1
N    127.30.0.0/16         [30] area: 0.0.0.0
                           via 10.10.0.2, eth2
                           via 10.0.0.2, eth1

============ OSPF router routing table =============
R    2.2.2.2               [10] area: 0.0.0.0, ASBR
                           via 10.0.0.2, eth1
R    3.3.3.3               [20] area: 0.0.0.0, ASBR
                           via 10.10.0.2, eth2
                           via 10.0.0.2, eth1

============ OSPF external routing table ===========
N E2 0.0.0.0/0             [10/1] tag: 0
                           via 10.0.0.2, eth1

```

Чтобы сделать симметричный нужен такой же кост на ro3
```
nterface eth1
 ip ospf mtu-ignore
 ip ospf network point-to-point
 ip ospf hello-interval 5
 ip ospf dead-interval 10
!
interface eth2
 ip ospf mtu-ignore
 ip ospf network point-to-point
 ip ospf hello-interval 5
 ip ospf dead-interval 10
 ip ospf cost 20
!

```
Пингуем туда и обратно
```
[root@ro1 ~]# tracepath 10.20.0.1
 1?: [LOCALHOST]                                         pmtu 1500
 1:  10.0.0.2                                              0.455ms
 1:  10.0.0.2                                              0.656ms
 2:  10.20.0.1                                             1.123ms reached
     Resume: pmtu 1500 hops 2 back 2

[root@ro3 ~]# tracepath 10.0.0.1
 1?: [LOCALHOST]                                         pmtu 1500
 1:  10.20.0.2                                             0.972ms
 1:  10.20.0.2                                             0.588ms
 2:  10.0.0.1                                              2.437ms reached
     Resume: pmtu 1500 hops 2 back 2

```
таблица маршрутизации
```
[root@ro1 ~]# vtysh -c 'sh ip ospf route'
============ OSPF network routing table ============
N    10.0.0.0/30           [10] area: 0.0.0.0
                           directly attached to eth1
N    10.10.0.0/30          [20] area: 0.0.0.0
                           directly attached to eth2
N    10.20.0.0/30          [20] area: 0.0.0.0
                           via 10.0.0.2, eth1
N    127.10.0.0/16         [10] area: 0.0.0.0
                           directly attached to lo
N    127.20.0.0/16         [20] area: 0.0.0.0
                           via 10.0.0.2, eth1
N    127.30.0.0/16         [30] area: 0.0.0.0
                           via 10.10.0.2, eth2
                           via 10.0.0.2, eth1

============ OSPF router routing table =============
R    2.2.2.2               [10] area: 0.0.0.0, ASBR
                           via 10.0.0.2, eth1
R    3.3.3.3               [20] area: 0.0.0.0, ASBR
                           via 10.10.0.2, eth2
                           via 10.0.0.2, eth1

============ OSPF external routing table ===========
N E2 0.0.0.0/0             [10/1] tag: 0
                           via 10.0.0.2, eth1
```
